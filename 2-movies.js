/*
 * NOTE: For all questions, the returned data must contain all the movie information including its name.

Q1. Find all the movies with total earnings more than $500M. 
Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
Q.3 Find all movies of the actor "Leonardo Dicaprio".
Q.4 Sort movies (based on IMDB rating)
    if IMDB ratings are same, compare totalEarning as the secondary metric.
Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
    drama > sci-fi > adventure > thriller > crime

*/

const movies = require("./dataset")

function totalEarnings(movies, earnings) {
    return Object.entries(movies).filter(([movie, movieDetails])=>{
        return +movieDetails.totalEarnings.replace("$", "").replace("M", "") > +earnings.replace("$", "").replace("M", "");
    }).reduce((accu, [movie, movieDetails]) => {
        accu[movie] = movieDetails;
        return accu;
    }, {})
}

// console.log(totalEarnings(movies,"$500M"));


function oscarNominatedAndHighEarning(movies, earnings, oscarCount) {

    return Object.entries(movies).filter(([movie, movieDetails]) => {
        return (+movieDetails.totalEarnings.replace("$", "").replace("M", "") > +earnings.replace("$", "").replace("M", "")) && movieDetails.oscarNominations > oscarCount;
    }).reduce((accu, [movie, movieDetails]) => {
        accu[movie] = movieDetails;
        return accu;
    }, {})

}

// console.log(oscarNominatedAndHighEarning(movies,"$500M",3));



function findMoviesOfActor(movies, actor) {
    return Object.entries(movies).filter(([movie, movieDetails]) => {
        return movieDetails.actors.includes(actor);
    }).reduce((accu, [movie, movieDetails]) => {
        accu[movie] = movieDetails;
        return accu;
    }, {})
}

// console.log(findMoviesOfActor(movies, "Leonardo Dicaprio"));

function sortMovies(movies) {
    return Object.entries(movies).sort(([movieA, movieDetailsA], [movieB, movieDetailsB]) => {
        if (movieDetailsA.imdbRating === movieDetailsB.imdbRating) {
            return +movieDetailsA.totalEarnings.replace("$", "").replace("M", "") - +movieDetailsB.totalEarnings.replace("$", "").replace("M", "")
        }
        return movieDetailsA.imdbRating - movieDetailsB.imdbRating;
    })
}
// console.log(sortMovies(movies));

// drama > sci - fi > adventure > thriller > crime
function findGenreGroup(movieGenre) {
    let order = {
        drama: 1,
        "sci - fi ": 2,
        adventure: 3,
        thriller: 4,
        crime: 5
    }
    let result;
    for (let index = 0; index < movieGenre.length; index++) {
        if (result === undefined) {
            result = movieGenre[index];
        } else {
            if (order[result] > order[movieGenre[index]]) {
                result = movieGenre[index];
            }
        }
    }
    return result;
}

function groupMovies(movies) {
    return Object.entries(movies).map(([movie, movieDetails]) => {
        movieDetails["movieName"] = movie;
        return movieDetails;
    }).reduce((accu, movieDetails) => {
        let genre = findGenreGroup(movieDetails.genre);
        if (accu[genre] === undefined) {
            accu[genre] = [movieDetails];
        } else {
            accu[genre].push(movieDetails);
        }
        return accu;
    }, {})
}


// console.log(groupMovies(movies));